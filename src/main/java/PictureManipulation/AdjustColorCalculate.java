
package main.java.PictureManipulation;

import java.util.TreeMap;

final public class AdjustColorCalculate {
  final TreeMap<Integer, TreeMap<Integer, Integer>> calculations;

  public AdjustColorCalculate() {
    this.calculations = new TreeMap<Integer, TreeMap<Integer, Integer>>();
  }

  public Integer getValue(final int adjustFactor, final int colorValue) {
    final TreeMap<Integer, Integer> colorValueWithFactors =
      this.calculations.get(adjustFactor);

    if (colorValueWithFactors != null) {
      final Integer value = colorValueWithFactors.get(colorValue);

      if (value != null) {
        return value;
      }
    }

    return null;
  }

  Integer getValue(final TreeMap<Integer, Integer> tm, final int colorValue) {
    final Integer value = tm.get(colorValue);

    if (value != null)  {
      return value;
    }

    return null;
  }

  public Integer setValue(final int adjustFactor, final int colorValue) {
    final TreeMap<Integer, Integer> colorValueWithFactors =
      this.calculations.get(adjustFactor);

    if (colorValueWithFactors == null) {
      final Integer value = this.calculate(adjustFactor, colorValue);

      this.setNewValue(adjustFactor, colorValue, value);
    } else {
      final Integer existingValue = colorValueWithFactors.get(colorValue);

      if (existingValue != null) {
        return existingValue;
      }

      final Integer value = this.calculate(adjustFactor, colorValue);

      colorValueWithFactors.put(colorValue, value);

      return value;
    }

    return null;
  }

  void setNewValue(final int adjustFactor, final int colorValue, final Integer value) {
    final TreeMap<Integer, Integer> calculation = new TreeMap<Integer, Integer>();

    calculation.put(colorValue, value);

    this.calculations.put(adjustFactor, calculation);
  }

  Integer calculate(final int adjustFactor, final int colorValue) {
    Integer value = (int)((adjustFactor / 100) * colorValue);

    return value + colorValue;
  }
}
