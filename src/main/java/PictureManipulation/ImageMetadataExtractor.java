
package main.java.PictureManipulation;

import java.awt.image.BufferedImage;
import java.awt.Dimension;
import java.lang.Exception;
import java.nio.file.Path;
import java.nio.file.Files;

import main.java.PictureManipulation.Shared.ImageSizeUnitMeasurement;
import main.java.PictureManipulation.Model.ImageMetadata;
import main.java.PictureManipulation.ReturnResult;

final public class ImageMetadataExtractor {
  public static ReturnResult<ImageMetadata> extract(
    final BufferedImage bufferedImage,
    final Path imageFilePath
  ) {
    try {
      final String imageFilename = imageFilePath.getFileName().toString();
      final String imageFileType = Files.probeContentType(imageFilePath);

      final long imageFileSize = Files.size(imageFilePath);
      final int imageWidth = bufferedImage.getWidth();
      final int imageHeight = bufferedImage.getHeight();

      final Dimension imageDimension = new Dimension(imageHeight, imageHeight);

      final ImageMetadata imageMetadata =
        new ImageMetadata(
          imageFilename,
          imageFileType,
          imageDimension,
          imageWidth,
          imageHeight,
          imageFileSize,
          ImageSizeUnitMeasurement.Measure.BYTE
        );

      return new ReturnResult<ImageMetadata>(imageMetadata);
    } catch (final Exception exception) {
      return new ReturnResult<ImageMetadata>(exception);
    }
  }
}
