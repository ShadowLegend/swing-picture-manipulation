
package main.java.PictureManipulation;

import java.beans.PropertyChangeSupport;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeListener;

import main.java.PictureManipulation.Model.ImageFile;
import main.java.PictureManipulation.DigitalPicture;

final public class AppState {
  ImageFile imageFile;
  DigitalPicture digitalPicture;
  BufferedImage croppedImage;

  PropertyChangeSupport watcher;

  public boolean isIndicatePixelInfoOnImageHover = true;

  public AppState() {
    this.imageFile = new ImageFile();

    this.digitalPicture = new DigitalPicture(this.imageFile);

    this.watcher = new PropertyChangeSupport(this);
  }

  public AppState(final String initialImageFilePath) {
    this.imageFile = new ImageFile(initialImageFilePath);

    this.digitalPicture = new DigitalPicture(this.imageFile);

    this.watcher = new PropertyChangeSupport(this);
  }

  public ImageFile getImageFile() { return this.imageFile; }

  public DigitalPicture getDigitalPicture() { return this.digitalPicture; }

  public void listenTo(final PropertyChangeListener listener) {
    this.watcher.addPropertyChangeListener(listener);
  }

  public void unlistenTo(final PropertyChangeListener listener) {
    this.watcher.removePropertyChangeListener(listener);
  }

  public PropertyChangeSupport dispatcher() { return this.watcher; }

  public void setCroppedImage(final BufferedImage croppedImage) {
    this.croppedImage = croppedImage;
  }

  public BufferedImage getCroppedImage() { return this.croppedImage; }
}
