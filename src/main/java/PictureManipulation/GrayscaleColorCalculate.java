
package main.java.PictureManipulation;

final public class GrayscaleColorCalculate {
  final static Integer[][][] memos = new Integer[255][255][255];

  public static int calculate(
    final int red,
    final int green,
    final int blue
  ) {
    if (GrayscaleColorCalculate.memos[red][green][blue] == null) {
      GrayscaleColorCalculate.memos [red][green][blue] =
        (red + green + blue) / 3;
    }

    return GrayscaleColorCalculate.memos[red][green][blue];
  }
}
