
package main.java.PictureManipulation.Shared;

final public class ImageSizeUnitMeasurement {
  public enum Measure {
    BYTE,
    MEGABYTE,
    GIGABYTE,
    TERABYTE,
  }
}
