
package main.java.PictureManipulation;

import java.lang.Exception;

final public class ReturnResult<T> {
  public final boolean isOk;
  public final Exception error;
  public final T payload;

  public ReturnResult() {
    this.isOk = true;
    this.error = null;
    this.payload = null;
  }

  public ReturnResult(final Exception error) {
    this.isOk = false;
    this.error = error;
    this.payload = null;
  }

  public ReturnResult(final T payload) {
    this.isOk = true;
    this.payload = payload;
    this.error = null;
  }
}
