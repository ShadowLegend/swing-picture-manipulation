
package main.java.PictureManipulation;

import java.io.File;
import java.util.UUID;
import java.lang.Exception;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.Color;

import main.java.PictureManipulation.AdjustColorCalculate;
import main.java.PictureManipulation.Pixel;
import main.java.PictureManipulation.Model.ImageFile;

final public class DigitalPicture {
  final static int MAX_RGB_VALUE = 255;

  Pixel[] pixels;
  ImageFile imageFile;

  AdjustColorCalculate adjustColorCalculate;

  BufferedImage imageGrayscale;

  boolean isPictureNegated;
  boolean isPictureGrayscaled;

  public DigitalPicture(final ImageFile imageFile) {
    this.imageFile = imageFile;

    this.adjustColorCalculate = new AdjustColorCalculate();

    this.isPictureNegated = false;
    this.isPictureGrayscaled = false;

    if (this.imageFile.isImageRead()) {
      this.generatePixel();
    }
  }

  public void setImageFile(final ImageFile imageFile) {
    this.imageFile = imageFile;

    this.imageGrayscale = null;

    this.isPictureNegated = false;
    this.isPictureGrayscaled = false;

    this.generatePixel();
  }

  int adjustColorValue(final int factor, final int colorValue) {
    final int amount = (int) ((factor / 100.0) * colorValue);
    final int newColorValue = colorValue + amount;

    // final Integer calculatedValue = this.adjustColorCalculate.getValue(factor, colorValue);

    // final int newColorValue =
    //   calculatedValue != null
    //     ? calculatedValue
    //     : this.adjustColorCalculate.setValue(factor, colorValue);

    return newColorValue > DigitalPicture.MAX_RGB_VALUE
      ? DigitalPicture.MAX_RGB_VALUE
      : newColorValue < 0 ? 0 : newColorValue;
  }
  
  public boolean isPictureCurrentlyNegated() { return this.isPictureNegated; }
  public boolean isPictureCurrentlyGrayscaled() { return this.isPictureGrayscaled; }

  public BufferedImage getImageGrayscaled() { return this.imageGrayscale; }
  public ImageFile getImageFile() { return this.imageFile; }

  void generatePixel() {
    final BufferedImage bufferedImage = this.imageFile.getBufferedImage();

    this.pixels = new Pixel[bufferedImage.getWidth() * bufferedImage.getHeight()];

    int loopIndex = 0;
    int innerLoopIndex = 0;

    int pixelArrayIndex = 0;

    for (; loopIndex < bufferedImage.getWidth(); ++loopIndex) {
      for (; innerLoopIndex < bufferedImage.getHeight(); ++innerLoopIndex) {
        this.pixels[pixelArrayIndex] =
          new Pixel(
            loopIndex,
            innerLoopIndex,
            bufferedImage.getRGB(loopIndex, innerLoopIndex)
          );

        ++pixelArrayIndex;
      }

      innerLoopIndex = 0;
    }
  }

  public Pixel[] getPixels() { return this.pixels; }

  public void adjustRed(final int factor) {
    for (Pixel pixel: this.pixels) {
      this.imageFile
        .getBufferedImage()
        .setRGB(
          pixel.getPositionX(),
          pixel.getPositionY(),
          (new Color(
            this.adjustColorValue(factor, pixel.getColor().getRed()),
            pixel.getColor().getGreen(),
            pixel.getColor().getBlue()
          )).getRGB()
        );

      pixel.setColor(new Color(this.imageFile.getBufferedImage().getRGB(pixel.getPositionX(), pixel.getPositionY())));
    }
  }

  public void adjustGreen(final int factor) {
    for (Pixel pixel: this.pixels) {
      this.imageFile
        .getBufferedImage()
        .setRGB(
          pixel.getPositionX(),
          pixel.getPositionY(),
          (new Color(
            pixel.getColor().getRed(),
            this.adjustColorValue(factor, pixel.getColor().getGreen()),
            pixel.getColor().getBlue()
          )).getRGB()
        );
      
      pixel.setColor(new Color(this.imageFile.getBufferedImage().getRGB(pixel.getPositionX(), pixel.getPositionY())));
    }
  }

  public void adjustBlue(final int factor) {
    for (Pixel pixel: this.pixels) {
      this.imageFile
        .getBufferedImage()
        .setRGB(
          pixel.getPositionX(),
          pixel.getPositionY(),
          (new Color(
            pixel.getColor().getRed(),
            pixel.getColor().getGreen(),
            this.adjustColorValue(factor, pixel.getColor().getBlue())
          )).getRGB()
        );

      pixel.setColor(new Color(this.imageFile.getBufferedImage().getRGB(pixel.getPositionX(), pixel.getPositionY())));
    }
  }

  public void negate() {
    for (Pixel pixel: this.pixels) {
      this.imageFile
        .getBufferedImage()
        .setRGB(
          pixel.getPositionX(),
          pixel.getPositionY(),
          (new Color(
            DigitalPicture.MAX_RGB_VALUE - pixel.getColor().getRed(),
            DigitalPicture.MAX_RGB_VALUE - pixel.getColor().getGreen(),
            DigitalPicture.MAX_RGB_VALUE - pixel.getColor().getBlue()
          )).getRGB()
        );

      pixel.setColor(new Color(this.imageFile.getBufferedImage().getRGB(pixel.getPositionX(), pixel.getPositionY())));
    }

    this.isPictureNegated = !this.isPictureNegated;
  }

  public void grayscale() {
    if (this.imageGrayscale == null) {
      this.imageGrayscale =
        new BufferedImage(
          this.imageFile.getBufferedImage().getWidth(),
          this.imageFile.getBufferedImage().getHeight(),
          BufferedImage.TYPE_INT_RGB
        );

      int grayscaleValue = 0;

      for (final Pixel pixel: this.pixels) {
        grayscaleValue =
          ( pixel.getColor().getRed()
          + pixel.getColor().getGreen()
          + pixel.getColor().getBlue()
          ) / 3;

        this.imageGrayscale.setRGB(
          pixel.getPositionX(),
          pixel.getPositionY(),
          (new Color(
            grayscaleValue,
            grayscaleValue,
            grayscaleValue
          )).getRGB()
        );

        pixel.setColor(new Color(this.imageFile.getBufferedImage().getRGB(pixel.getPositionX(), pixel.getPositionY())));
      }
    }

    if (this.imageGrayscale != null) {
      this.isPictureGrayscaled = !this.isPictureGrayscaled;
    }
  }

  public BufferedImage scale(final int newWidth, final int newHeight) {
    final BufferedImage scaledImage =
      new BufferedImage(
        newWidth,
        newHeight,
        this.imageFile.getBufferedImage().getType()
      );

    // final int[] newPixels = new int[newWidth * newHeight];

    final double xRatio = this.imageFile.getMetadata().width / (double) newWidth;
    final double yRatio = this.imageFile.getMetadata().height / (double) newHeight;

    double x = 0;
    double y = 0;

    int loopIndex = 0;
    int innerLoopIndex = 0;
    // int pixelArrayIndex = 0;

    for (; loopIndex < newWidth; ++loopIndex) {
      for (; innerLoopIndex < newHeight; ++innerLoopIndex) {
        x = Math.floor(loopIndex * xRatio);
        y = Math.floor(innerLoopIndex * yRatio);

        scaledImage.setRGB(
          loopIndex,
          innerLoopIndex,
          this.imageFile.getBufferedImage().getRGB((int) x, (int) y)
        );
        // newPixels[pixelArrayIndex] = this.imageFile.getBufferedImage().getRGB((int) x, (int) y);

        // ++pixelArrayIndex;
      }

      innerLoopIndex = 0;
    }

    // scaledImage.setRGB(0, 0, newWidth, newHeight, newPixels, 0, 1);

    return scaledImage;
  }

  public void saveImage(final String path) {
    try {
      final File file =
        new File(String.format("%s/%s.jpg", path, UUID.randomUUID()));

      if (this.isPictureGrayscaled) {
        ImageIO.write(this.imageGrayscale, "jpg", file);

        return;
      }

      ImageIO.write(this.imageFile.getBufferedImage(), "jpg", file);

      return;
    } catch (Exception error) {
      System.out.println(error.toString());
    }
  }
}
