package main.java.PictureManipulation;

import java.beans.PropertyChangeListener;

public interface AppStateChangeAction<T extends PropertyChangeListener> {
  public abstract void listenTo(final T listener);

  public abstract void unlistenTo(final T listener);
}
