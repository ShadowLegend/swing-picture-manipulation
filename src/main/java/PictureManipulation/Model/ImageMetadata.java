
package main.java.PictureManipulation.Model;

import java.awt.Dimension;

import main.java.PictureManipulation.Shared.ImageSizeUnitMeasurement;

final public class ImageMetadata {
  public final String name;
  public final String type;
  public final Dimension dimension;
  public final int width;
  public final int height;
  public final long size;

  final ImageSizeUnitMeasurement.Measure imageSizeUnitMeasurement;

  public ImageMetadata(
    final String name,
    final String type,
    final Dimension dimension,
    final int width,
    final int height,
    final long size,
    final ImageSizeUnitMeasurement.Measure imageSizeUnitMeasurement
  ) {
    this.name = name;
    this.type = type;
    this.dimension = dimension;
    this.width = width;
    this.height = height;
    this.size = size;
    this.imageSizeUnitMeasurement = imageSizeUnitMeasurement;
  }

  public String sizeUnitMeasurement() {
    switch (this.imageSizeUnitMeasurement) {
      case BYTE: {
        return "bytes";
      }

      case GIGABYTE: {
        return "gigabytes";
      }

      case MEGABYTE: {
        return "megabytes";
      }

      default: {
        return "";
      }
    }
  }

  public void show() {
    System.out.println(String.format("Name: %s", this.name));
    System.out.println(String.format("Type: %s", this.type));
    System.out.println(String.format(
      "Dimension: %sx%s",
      this.dimension.getWidth(),
      this.dimension.getHeight()
    ));
    System.out.println(String.format("Size: %s bytes", this.size));
  }
}
