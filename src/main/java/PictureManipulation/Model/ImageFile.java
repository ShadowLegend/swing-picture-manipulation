
package main.java.PictureManipulation.Model;

import java.io.File;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

import main.java.PictureManipulation.ReturnResult;
import main.java.PictureManipulation.Model.ImageMetadata;
import main.java.PictureManipulation.ImageMetadataExtractor;

final public class ImageFile {
  Path imageFilePath;

  BufferedImage bufferedImage;

  ImageMetadata imageMetadata;

  boolean isImageSuccessfullyRead;

  public ImageFile() {
    this.isImageSuccessfullyRead = false;
  }

  public ImageFile(final String imageFilePath) {
    try {
      this.imageFilePath = Paths.get(imageFilePath);
      this.bufferedImage = ImageIO.read(this.imageFilePath.toFile());

      this.imageMetadata = null;

      ReturnResult<ImageMetadata> result =
        ImageMetadataExtractor.extract(this.bufferedImage, this.imageFilePath);

      if (result.isOk) {
        this.imageMetadata = result.payload;

        this.isImageSuccessfullyRead = true;
      }
    } catch (final Exception exception) {
      exception.printStackTrace();

      this.isImageSuccessfullyRead = false;
    }
  }

  public ReturnResult<String> openImage(final String imageFilePath) {
    return new ReturnResult<String>();
  }

  public ReturnResult<ImageMetadata> openImage(final File imageFile) {
    try {
      this.imageFilePath = imageFile.toPath();
      this.bufferedImage = ImageIO.read(imageFile);

      ReturnResult<ImageMetadata> result =
        ImageMetadataExtractor.extract(this.bufferedImage, this.imageFilePath);

      if (result.isOk) {
        this.imageMetadata = result.payload;
        this.isImageSuccessfullyRead = true;
      }

      return result;
    } catch (final Exception exception) {
      exception.printStackTrace();

      this.isImageSuccessfullyRead = false;

      return new ReturnResult<ImageMetadata>(exception);
    }
  }

  public ImageMetadata getMetadata() { return this.imageMetadata; }

  public BufferedImage getBufferedImage() { return this.bufferedImage; }

  public boolean isImageRead() { return this.isImageSuccessfullyRead; }
}
