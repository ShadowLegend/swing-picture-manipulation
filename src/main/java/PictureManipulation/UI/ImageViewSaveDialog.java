
package main.java.PictureManipulation.UI;

import java.awt.Component;
import javax.swing.JPanel;
import javax.swing.BoxLayout;

import main.java.PictureManipulation.UI.UIComponent;

public class ImageViewSaveDialog<T extends Component> extends UIComponent<JPanel> {

  public ImageViewSaveDialog(final T imageView) {
    this.component = new JPanel();

    this.component.setLayout(new BoxLayout(this.component, BoxLayout.Y_AXIS));

    this.component.add(imageView);
  }

  @Override
  public JPanel internalComponent() { return this.component; }

  @Override
  public void update() {}

  @Override
  protected void setup() {}
}
