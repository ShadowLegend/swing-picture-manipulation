
package main.java.PictureManipulation.UI;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.BoxLayout;
import java.awt.image.BufferedImage;
import java.awt.Image;

import main.java.PictureManipulation.UI.UIComponent;

final public class ImageScaleDisplayDialog extends UIComponent<JPanel> {
  final BufferedImage bufferedImage;

  public ImageScaleDisplayDialog(final BufferedImage bufferedImage) {
    this.bufferedImage = bufferedImage;

    this.component = new JPanel();

    this.component.setLayout(
      new BoxLayout(this.component, BoxLayout.PAGE_AXIS));

    final Image image = ((Image) this.bufferedImage).getScaledInstance(
      this.bufferedImage.getWidth(),
      this.bufferedImage.getHeight(),
      Image.SCALE_SMOOTH
    );

    final JLabel imageDisplay = new JLabel(new ImageIcon(image));

    this.component.add(imageDisplay);
  }

  public void update() {}

  protected void setup() {}
  
  public JPanel internalComponent() { return this.component; }
}
