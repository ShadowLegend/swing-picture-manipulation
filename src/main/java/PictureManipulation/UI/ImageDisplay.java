
package main.java.PictureManipulation.UI;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import main.java.PictureManipulation.Action.ActionIdentifier;
import main.java.PictureManipulation.UI.UIComponent;
import main.java.PictureManipulation.Model.ImageFile;
import main.java.PictureManipulation.UI.ImageHoverPixelIndicate;
import main.java.PictureManipulation.UI.ImageHoverPixelIndicator;
import main.java.PictureManipulation.AppState;

final public class ImageDisplay extends UIComponent<JLayeredPane> {
  final static int defaultImageWidth = 500;
  final static int defaultImageHeight = 380;

  Image image;

  JLabel imageDisplay;

  BufferedImage scaledFitLabelImage;
  
  ImageHoverPixelIndicate hoverPixelIndicate;

  final AppState appState;

  final private class ImageChangeReaction implements PropertyChangeListener {
    @Override
    public void propertyChange(PropertyChangeEvent event) {
      switch (event.getPropertyName()) {
        case ActionIdentifier.ADJUST_IMAGE_COLOR:
        case ActionIdentifier.IMAGE_NEGATE_TOGGLE:
        case ActionIdentifier.OPENED_IMAGE_FILE: {
          ImageDisplay.this.update((ImageFile) event.getNewValue());

          break;
        }

        case ActionIdentifier.IMAGE_GRAYSCALE_TOGGLE: {
          ImageDisplay.this.update((BufferedImage) event.getNewValue());

          break;
        }

        default: {break;}
      }
    }
  }

  public ImageChangeReaction reaction() { return new ImageChangeReaction(); }

  public ImageDisplay(final ImageFile imageFile, final AppState appState) {
    this.appState = appState;
    this.component = new JLayeredPane();
    this.component.setPreferredSize(
      new Dimension(ImageDisplay.defaultImageWidth, ImageDisplay.defaultImageHeight));
    this.component.setBorder(BorderFactory.createLineBorder(Color.black));

    if (imageFile.isImageRead()) {
      this.setImage(imageFile);
      this.imageDisplay = new JLabel(new ImageIcon(this.image));
    } else {
      this.imageDisplay = new JLabel();
    }

    this.imageDisplay.setSize(ImageDisplay.defaultImageWidth, ImageDisplay.defaultImageHeight);

    this.component.add(this.imageDisplay, Integer.valueOf(0));

    final ImageHoverPixelIndicator imageHoverPixelIndicator =
      new ImageHoverPixelIndicator();

    this.hoverPixelIndicate =
      new ImageHoverPixelIndicate(
        imageHoverPixelIndicator,
        this.scaledFitLabelImage,
        this.appState
      );

    this.component.add(imageHoverPixelIndicator.internalComponent(), Integer.valueOf(1));
    this.component.addMouseMotionListener(this.hoverPixelIndicate);
    this.component.addMouseListener(this.hoverPixelIndicate);
  }

  void setImage(
    final ImageFile imageFile,
    final int imageWidth,
    final int imageHeight
  ) {
    BufferedImage bufferedImage = imageFile.getBufferedImage();

    this.image = ((Image) bufferedImage).getScaledInstance(
      imageWidth,
      imageHeight,
      Image.SCALE_SMOOTH
    );
  }

  void setImage(final ImageFile imageFile) {
    BufferedImage bufferedImage = imageFile.getBufferedImage();

    this.image = ((Image) bufferedImage).getScaledInstance(
      ImageDisplay.defaultImageWidth,
      ImageDisplay.defaultImageHeight,
      Image.SCALE_DEFAULT
    );

    this.scaledFitLabelImage =
      new BufferedImage(
        this.image.getWidth(null),
        this.image.getHeight(null),
        BufferedImage.TYPE_INT_ARGB
      );

    final Graphics2D graphic = this.scaledFitLabelImage.createGraphics();

    graphic.drawImage(this.image, 0, 0, null);

    graphic.dispose();
  }

  void setImage(final BufferedImage imageBuffer) {
    this.image = ((Image) imageBuffer).getScaledInstance(
      ImageDisplay.defaultImageWidth,
      ImageDisplay.defaultImageHeight,
      Image.SCALE_DEFAULT
    );

    this.scaledFitLabelImage =
      new BufferedImage(
        this.image.getWidth(null),
        this.image.getHeight(null),
        BufferedImage.TYPE_INT_ARGB
      );

    final Graphics2D graphic = this.scaledFitLabelImage.createGraphics();

    graphic.drawImage(this.image, 0, 0, null);

    graphic.dispose();
  }

  public JLayeredPane internalComponent() { return this.component; }

  public void update() {
    this.component.revalidate();
    this.component.updateUI();
  }

  public void update(final ImageFile imageFile) {
    this.setImage(imageFile);

    this.imageDisplay.setIcon(new ImageIcon(this.image));

    this.hoverPixelIndicate.setBufferedImage(this.scaledFitLabelImage);

    this.component.revalidate();
    this.component.updateUI();
  }

  public void update(final BufferedImage imageBuffer) {
    this.setImage(imageBuffer);

    this.imageDisplay.setIcon(new ImageIcon(this.image));

    this.hoverPixelIndicate.setBufferedImage(this.scaledFitLabelImage);

    this.component.revalidate();
    this.component.updateUI();
  }

  protected void setup() {}
}
