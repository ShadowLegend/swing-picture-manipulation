
package main.java.PictureManipulation.UI;

import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.text.AbstractDocument;
import java.awt.image.BufferedImage;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import javax.swing.JFrame;
import java.util.regex.Pattern;
import javax.swing.BoxLayout;

import main.java.PictureManipulation.UI.ImageScaleDisplayDialog;
import main.java.PictureManipulation.UI.UIComponent;
import main.java.PictureManipulation.AppState;

final public class ImageDimensionInput extends UIComponent<JPanel> {
  final int inputColumnNumber = 5;
  final int minWidth;
  final int minHeight;
  final AppState appState;

  final private class DocumentNumberFilter extends DocumentFilter {
    final int minValue;

    final private Pattern regexPattern = Pattern.compile("[0-9]+");

    public DocumentNumberFilter(final int minValue) {
      this.minValue = minValue;
    }

    @Override
    public void insertString(
      DocumentFilter.FilterBypass filterBypass,
      int offset,
      String string,
      AttributeSet attributeSet
    ) {
      if (string == null) { return; }

      try {
        if (this.regexPattern.matcher(string).matches()) {
          final int value = Integer.valueOf(string);

          if (value < this.minValue) {
            // return;
          }

          super.insertString(filterBypass, offset, string, attributeSet);
        }
      } catch (BadLocationException error) {
        error.printStackTrace();
      }
    }

    @Override
    public void replace(
      DocumentFilter.FilterBypass filterBypass,
      int offset,
      int length,
      String string,
      AttributeSet attributeSet
    ) {
      if (string == null) { return; }

      try {
        if (this.regexPattern.matcher(string).matches()) {
          final int value = Integer.valueOf(string);

          if (value < this.minValue) {
            // return;
          }

          super.insertString(filterBypass, offset, string, attributeSet);
        }
      } catch (BadLocationException error) {
        error.printStackTrace();
      }
    }
  }

  public ImageDimensionInput(final int minWidth, final int minHeight, final AppState appState) {
    this.minWidth = minWidth;
    this.minHeight = minHeight;

    this.component = new JPanel();
    this.appState = appState;

    this.component.setLayout(new BoxLayout(this.component, BoxLayout.PAGE_AXIS));

    this.component.add(new JLabel("Crop Image Dimension: "));

    final JPanel textField = new JPanel();

    final JTextField widthInput = new JTextField(this.inputColumnNumber);
    final JTextField heightInput = new JTextField(this.inputColumnNumber);

    ((AbstractDocument) widthInput.getDocument()).setDocumentFilter(
      new DocumentNumberFilter(this.minWidth));

    ((AbstractDocument) heightInput.getDocument()).setDocumentFilter(
      new DocumentNumberFilter(this.minHeight));

    widthInput.setText(String.valueOf(this.minWidth));
    heightInput.setText(String.valueOf(this.minHeight));

    textField.add(widthInput);
    textField.add(new JLabel("X"));
    textField.add(heightInput);

    this.component.add(textField);

    final JButton previewScaleButton = new JButton("Preview new cropping image");

    previewScaleButton.addActionListener((event) -> {
      final JFrame jframe = new JFrame();

      final int newWidth = Integer.valueOf(widthInput.getText());
      final int newHeight = Integer.valueOf(heightInput.getText());

      final BufferedImage bufferedImage =
        this.appState.getDigitalPicture().scale(newWidth, newHeight);

      jframe.setSize(newWidth, newHeight);

      jframe.add((new ImageScaleDisplayDialog(bufferedImage)).internalComponent());

      jframe.setVisible(true);
      jframe.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    });

    this.component.add(previewScaleButton);
  }

  public void update() {}

  public void setup() {}

  public JPanel internalComponent() { return this.component; }
}
