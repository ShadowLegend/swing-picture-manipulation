
package main.java.PictureManipulation.UI;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

import main.java.PictureManipulation.UI.ImageHoverPixelIndicator;
import main.java.PictureManipulation.AppState;

final public class ImageHoverPixelIndicate implements MouseMotionListener, MouseListener {
  final ImageHoverPixelIndicator indicator;
  final AppState appState;

  BufferedImage bufferedImage;

  public ImageHoverPixelIndicate(
    final ImageHoverPixelIndicator indicator,
    final BufferedImage bufferedImage,
    final AppState appState
  ) {
    this.indicator = indicator;
    this.bufferedImage = bufferedImage;
    this.appState = appState;
  }

  @Override
  public void mouseDragged(MouseEvent e) {}

  @Override
  public void mouseMoved(MouseEvent event) {
    if (this.appState.isIndicatePixelInfoOnImageHover) {
      final int x = event.getX();
      final int y = event.getY();

      final Color color = new Color(this.bufferedImage.getRGB(x, y));

      this.indicator.setLocation(x, y);
      this.indicator.setRGBValue(color.getRed(), color.getGreen(), color.getBlue());

      this.indicator.update();
    }
  }

  @Override
  public void mouseClicked(MouseEvent e) {}

  @Override
  public void mousePressed(MouseEvent e) {}

  @Override
  public void mouseReleased(MouseEvent e) {}

  @Override
  public void mouseEntered(MouseEvent e) {
    if (this.appState.isIndicatePixelInfoOnImageHover) {
      this.indicator.setVisible(true);
      this.indicator.update();
    }
  }

  @Override
  public void mouseExited(MouseEvent e) {
    if (this.appState.isIndicatePixelInfoOnImageHover) {
      this.indicator.setVisible(false);
      this.indicator.update();
    }
  }

  public void setBufferedImage(final BufferedImage bufferedImage) {
    this.bufferedImage = bufferedImage;
  }
}
