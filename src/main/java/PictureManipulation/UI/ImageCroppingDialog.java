
package main.java.PictureManipulation.UI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JFrame;

import main.java.PictureManipulation.UI.UIComponent;
import main.java.PictureManipulation.AppState;

final public class ImageCroppingDialog extends UIComponent<JPanel> {
  final AppState appState;

  int imageWidth;
  int imageHeight;

  int cropStartX = 0;
  int cropStartY = 0; 
  int cropEndX = 0;
  int cropEndY = 0;

  boolean isCropping = false;

  final JPanel cropBox;
  final JLabel imageLayer;

  final class ImageCropActionListener implements MouseListener, MouseMotionListener {
    @Override
    public void mouseDragged(MouseEvent event) {
      if (ImageCroppingDialog.this.isCropping) {
        ImageCroppingDialog.this.cropEndX =
          event.getX() >= ImageCroppingDialog.this.imageWidth
            ? ImageCroppingDialog.this.imageWidth
            : event.getX();
        ImageCroppingDialog.this.cropEndY =
          event.getY() >= ImageCroppingDialog.this.imageHeight
            ? ImageCroppingDialog.this.imageHeight
            : event.getY();

        int width =
          ImageCroppingDialog.this.cropEndX - ImageCroppingDialog.this.cropStartX;
        int height =
          ImageCroppingDialog.this.cropEndY - ImageCroppingDialog.this.cropStartY;

        width = width < 0 ? 1 : width;
        height = height < 0 ? 1 : height;

        ImageCroppingDialog.this.cropBox.setSize(width, height);
      }
    }

    @Override
    public void mouseMoved(MouseEvent e) {}

    @Override
    public void mouseClicked(MouseEvent e) {}

    @Override
    public void mousePressed(MouseEvent event) {
      ImageCroppingDialog.this.isCropping = true;

      ImageCroppingDialog.this.resetCroppingLocation();
      ImageCroppingDialog.this.cropBox.setSize(0, 0);

      ImageCroppingDialog.this.cropStartX = event.getX();
      ImageCroppingDialog.this.cropStartY = event.getY();

      ImageCroppingDialog.this.cropBox.setLocation(
        ImageCroppingDialog.this.cropStartX,
        ImageCroppingDialog.this.cropStartY
      );
    }

    @Override
    public void mouseReleased(MouseEvent e) {
      ImageCroppingDialog.this.isCropping = false;
    }

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}
  }

  public ImageCroppingDialog(final AppState appState) {
    this.appState = appState;
    this.imageWidth = this.appState.getImageFile().getMetadata().width;
    this.imageHeight = this.appState.getImageFile().getMetadata().height;

    this.component = new JPanel();
    this.component.setLayout(new BoxLayout(this.component, BoxLayout.Y_AXIS));

    final JLayeredPane layeredPane = new JLayeredPane();

    layeredPane.setPreferredSize(
      new Dimension(this.imageWidth, this.imageHeight));

    final ImageCropActionListener imageCropActionListenter =
      new ImageCropActionListener();

    this.cropBox = new JPanel();
    this.cropBox.setSize(this.imageWidth, this.imageHeight);
    this.cropBox.setBorder(BorderFactory.createLineBorder(Color.red));
    this.cropBox.setLocation(0, 0);
    this.cropBox.setBackground(new Color(0, 0, 0, 0));
    this.cropBox.setOpaque(true);
    this.cropBox.setSize(0, 0);

    this.imageLayer = new JLabel();
    this.imageLayer.setSize(this.imageWidth, this.imageHeight);
    this.imageLayer.setIcon(
      new ImageIcon((Image) this.appState.getImageFile().getBufferedImage()));

    this.imageLayer.addMouseListener(imageCropActionListenter);
    this.imageLayer.addMouseMotionListener(imageCropActionListenter);

    layeredPane.add(this.imageLayer, JLayeredPane.DEFAULT_LAYER);
    layeredPane.add(this.cropBox, JLayeredPane.POPUP_LAYER);

    final JButton cropButton = new JButton("Crop");

    cropButton.addActionListener((event) -> {
      if (this.cropStartX != 0 && this.cropEndY != 0) {
        final int width = Math.abs(this.cropEndX - this.cropStartX);
        final int height = Math.abs(this.cropEndY - this.cropStartY);

        final BufferedImage croppedBufferedImage =
          this.appState.getImageFile().getBufferedImage().getSubimage(
            this.cropStartX,
            this.cropStartY,
            width,
            height
          );

        this.appState.setCroppedImage(croppedBufferedImage);

        final JFrame frame = new JFrame();

        frame.setVisible(true);
        frame.setSize(width, height);
        frame.add(new JLabel(new ImageIcon((Image) croppedBufferedImage)));
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
      }
    });

    this.component.add(layeredPane);
    this.component.add(cropButton);
  }

  public void resetCroppingLocation() {
    this.cropStartX = 0;
    this.cropEndX = 0;
    this.cropStartY = 0;
    this.cropEndY = 0;
  }

  public void crop() {}

  @Override
  public JPanel internalComponent() { return this.component; }

  @Override
  public void update() {}

  @Override
  protected void setup() {}
}
