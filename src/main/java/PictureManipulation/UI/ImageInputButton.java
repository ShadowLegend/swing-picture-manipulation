
package main.java.PictureManipulation.UI;

import java.awt.Button;

import main.java.PictureManipulation.UI.UIComponent;

final public class ImageInputButton extends UIComponent<Button> {
  String label;

  public ImageInputButton(final String label) {
    this.label = label;
    this.component = new Button(label);
  }

  public Button internalComponent() { return this.component; }

  protected void setup() {}

  public void update() {}
}
