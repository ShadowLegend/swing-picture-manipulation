
package main.java.PictureManipulation.UI;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.BoxLayout;

import main.java.PictureManipulation.Action.ActionIdentifier;
import main.java.PictureManipulation.Model.ImageMetadata;
import main.java.PictureManipulation.Model.ImageFile;
import main.java.PictureManipulation.UI.UIComponent;

final public class ImageMetadataDisplay extends UIComponent<JPanel> {
  ImageMetadata imageMetadata;

  final private class ImageChangeReaction implements PropertyChangeListener {
    @Override
    public void propertyChange(PropertyChangeEvent event) {
      switch (event.getPropertyName()) {
        case ActionIdentifier.OPENED_IMAGE_FILE: {
          ImageMetadataDisplay.this.update(
            ((ImageFile) event.getNewValue()).getMetadata());

          break;
        }

        default: {
          break;
        }
      }
    }
  }

  public ImageChangeReaction reaction() { return new ImageChangeReaction(); }

  public ImageMetadataDisplay(final ImageMetadata imageMetadata) {
    this.component = new JPanel();

    this.component.setLayout(new BoxLayout(this.component, BoxLayout.Y_AXIS));

    this.imageMetadata = imageMetadata;

    this.setup();
  }

  protected void setup() {
    JLabel filename =
      new JLabel(
        this.imageMetadata != null
          ? String.format("Filename: %s", this.imageMetadata.name)
          : ""
      );

    JLabel fileType =
      new JLabel(
        this.imageMetadata != null
          ? String.format("File type: %s", this.imageMetadata.type)
          : ""
      );

    JLabel dimension =
      new JLabel(
        this.imageMetadata != null
        ? String.format(
            "Dimension: %s x %s",
            this.imageMetadata.dimension.getWidth(),
            this.imageMetadata.dimension.getHeight()
          )
        : ""
      );

    JLabel fileSize = new JLabel(
      this.imageMetadata != null
      ? String.format(
          "File size: %s %s",
          this.imageMetadata.size,
          this.imageMetadata.sizeUnitMeasurement()
        )
      : ""
    );

    this.component.add(filename);
    this.component.add(fileType);
    this.component.add(dimension);
    this.component.add(fileSize);
  }

  public JPanel internalComponent() { return this.component; }

  public void update() {}

  public void update(final ImageMetadata imageMetadata) {
    this.imageMetadata = imageMetadata;

    ((JLabel) (this.component
      .getComponent(0)))
      .setText(String.format("Filename: %s", this.imageMetadata.name));

    ((JLabel) (this.component
      .getComponent(1)))
      .setText(String.format("File type: %s", this.imageMetadata.type));

    ((JLabel) (this.component
      .getComponent(2)))
      .setText(String.format(
        "Dimension: %s x %s",
        this.imageMetadata.dimension.getWidth(),
        this.imageMetadata.dimension.getHeight()
      ));

    ((JLabel) (this.component
      .getComponent(3)))
      .setText(String.format(
        "File size: %s %s",
        this.imageMetadata.size,
        this.imageMetadata.sizeUnitMeasurement()
      ));

    this.component.revalidate();
    this.component.updateUI();
  }
}
