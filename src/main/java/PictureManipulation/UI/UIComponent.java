
package main.java.PictureManipulation.UI;

import java.awt.Component;

public abstract class UIComponent<T extends Component> {
  protected T component;

  public abstract T internalComponent();

  public abstract void update();

  protected abstract void setup();
}
