
package main.java.PictureManipulation.UI;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JSlider;

import main.java.PictureManipulation.UI.UIComponent;

final public class RGBSlider extends UIComponent<JPanel> {
  public RGBSlider(final String sliderLabel) {
    this.setup(sliderLabel);
  }

  public RGBSlider(
    final String sliderLabel,
    final int minValue,
    final int maxValue,
    final int initialValue
  ) {
    this.setup(sliderLabel, minValue, maxValue, initialValue);
  }

  public JPanel internalComponent() { return this.component; }

  public void update() {}

  protected void setup() {}

  protected void setup(final String sliderLabel) {
    this.component = new JPanel();

    this.createSlider(sliderLabel, -100, 100, 0);
  }

  protected void setup(
    final String sliderLabel,
    final int minValue,
    final int maxValue,
    final int initialValue
  ) {
    this.component = new JPanel();

    this.createSlider(sliderLabel, minValue, maxValue, initialValue);
  }

  void createSlider(
    final String sliderLabel,
    final int minValue,
    final int maxValue,
    final int initialValue
  ) {
    final JLabel sliderLabelComponent = new JLabel(sliderLabel);

    final JSlider slider =
      new JSlider(JSlider.HORIZONTAL, minValue, maxValue, initialValue);
    
    slider.setMajorTickSpacing(50);
    slider.setMinorTickSpacing(10);
    slider.setPaintTicks(true);
    slider.setPaintLabels(true);
    slider.setSnapToTicks(true);

    this.component.add(sliderLabelComponent);
    this.component.add(slider);
  }

  public void resetSlider() {
    ((JSlider) this.component.getComponent(1)).setValue(0);
  }
}
