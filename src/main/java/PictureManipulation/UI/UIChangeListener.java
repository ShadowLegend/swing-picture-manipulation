
package main.java.PictureManipulation.UI;

import java.beans.PropertyChangeListener;

public interface UIChangeListener extends PropertyChangeListener {
  public abstract void listenTo(final PropertyChangeListener listener);
  public abstract void unlistenTo(final PropertyChangeListener listener);
}
