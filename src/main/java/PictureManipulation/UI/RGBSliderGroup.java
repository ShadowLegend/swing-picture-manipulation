
package main.java.PictureManipulation.UI;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.util.TreeMap;

import main.java.PictureManipulation.Action.ActionIdentifier;

import main.java.PictureManipulation.AppState;
import main.java.PictureManipulation.UI.RGBSlider;
import main.java.PictureManipulation.UI.UIComponent;

final public class RGBSliderGroup extends UIComponent<JPanel> {
  final int minSliderValue;
  final int maxSliderValue;
  final int initialSliderValue;
  AppState appState;

  final String[] colorLabels = new String[] {"red", "green", "blue"};

  final TreeMap<String, RGBSlider> rgbSliders;

  public RGBSliderGroup(final AppState appState) {
    this.appState = appState;

    this.component = new JPanel();

    this.component.setLayout(new BoxLayout(this.component, BoxLayout.PAGE_AXIS));

    this.minSliderValue = -100;
    this.maxSliderValue = 100;
    this.initialSliderValue = 0;

    this.rgbSliders = new TreeMap<String, RGBSlider>();

    for (final String colorLabel: this.colorLabels) {
      final RGBSlider rgbSlider =
        new RGBSlider(
          colorLabel.toUpperCase(),
          this.minSliderValue,
          this.maxSliderValue,
          this.initialSliderValue
        );

      final JSlider slider =
        (JSlider) rgbSlider.internalComponent().getComponent(1);

      slider.addChangeListener(new SliderOnChange(colorLabel, this.appState));

      this.rgbSliders.put(colorLabel, rgbSlider);

      this.component.add(this.rgbSliders.get(colorLabel).internalComponent());
    }
  }

  private final class SliderOnChange implements ChangeListener {
    final String sliderId;
    final AppState appState;

    public SliderOnChange(final String sliderId, final AppState appState) {
      this.sliderId = sliderId;
      this.appState = appState;
    }

    public void stateChanged(ChangeEvent event) {
      final JSlider changedSlider = (JSlider) event.getSource();

      if (!this.appState.getImageFile().isImageRead()) {
        changedSlider.setValue(0);

        return;
      }

      if (!changedSlider.getValueIsAdjusting()) {
        final int value = changedSlider.getValue();

        switch (this.sliderId) {
          case "red": {
            this.appState.getDigitalPicture().adjustRed(value);

            break;
          }

          case "green": {
            this.appState.getDigitalPicture().adjustGreen(value);

            break;
          }

          case "blue": {
            this.appState.getDigitalPicture().adjustBlue(value);

            break;
          }

          default: {
            break;
          }
        }

        this.appState.dispatcher().firePropertyChange(
          ActionIdentifier.ADJUST_IMAGE_COLOR,
          null,
          this.appState.getImageFile()
        );
      }
    }
  }

  final private class ImageChangeReaction implements PropertyChangeListener {
    @Override
    public void propertyChange(PropertyChangeEvent event) {
      switch (event.getPropertyName()) {
        case ActionIdentifier.OPENED_IMAGE_FILE: {
          for (final String colorLabel: RGBSliderGroup.this.colorLabels) {
            RGBSliderGroup.this.rgbSliders.get(colorLabel).resetSlider();
          }

          RGBSliderGroup.this.update();
        }

        default: {break;}
      }
    }
  }

  public ImageChangeReaction reaction() { return new ImageChangeReaction(); }

  public JPanel internalComponent() { return this.component; }

  public void update() {}

  protected void setup() {
    this.component.revalidate();
    this.component.updateUI();
  }
}
