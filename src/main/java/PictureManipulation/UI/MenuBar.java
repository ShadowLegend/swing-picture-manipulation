
package main.java.PictureManipulation.UI;

import javax.swing.JMenuBar;
import javax.swing.JFileChooser;

import main.java.PictureManipulation.UI.MenuBarMenu;
import main.java.PictureManipulation.UI.UIComponent;

final public class MenuBar extends UIComponent<JMenuBar> {
  JFileChooser fileChooser;

  public MenuBar() { this.setup(); }

  public MenuBar(final MenuBarMenu menu) {
    this.setup();

    this.component.add(menu.internalComponent());
  }

  public MenuBar(final MenuBarMenu[] menus) {
    this.setup();

    for (final MenuBarMenu menu : menus) {
      this.component.add(menu.internalComponent());
    }
  }

  public void addMenu(final MenuBarMenu menu) {
    this.component.add(menu.internalComponent());
  }

  public JMenuBar internalComponent() { return this.component; }

  protected void setup() {
    this.fileChooser = new JFileChooser();

    this.component = new JMenuBar();
  }

  public void update() {}

  public JFileChooser getFileChooser() { return this.fileChooser; }
}
