
package main.java.PictureManipulation.UI;

import javax.swing.JPanel;
import javax.swing.JLabel;

import java.awt.Color;

import javax.swing.BoxLayout;

import main.java.PictureManipulation.UI.UIComponent;
import main.java.PictureManipulation.UI.ImageDisplay;

final public class ImageHoverPixelIndicator extends UIComponent<JPanel> {
  final static int indicatorWidth = 170;
  final static int indicatorHeight = 38;

  final int locationValueElementIndex = 0;
  final int rgbValueElementIndex = 1;

  ImageHoverPixelIndicator() {
    this.component = new JPanel();
    // this.component.setOpaque(false);
    this.component.setBackground(Color.white);
    this.component.setSize(
      ImageHoverPixelIndicator.indicatorWidth,
      ImageHoverPixelIndicator.indicatorHeight
    );
    this.component.setLayout(new BoxLayout(this.component, BoxLayout.Y_AXIS));
    this.component.setVisible(false);

    this.component.add(new JLabel("Location: (0, 0)"));
    this.component.add(new JLabel("RGB Value: (0, 0, 0)"));

    this.component.setLocation(0, 0);
  }

  @Override
  public JPanel internalComponent() { return this.component; }

  @Override
  public void update() {
    this.component.revalidate();
    this.component.updateUI();
  }

  @Override
  protected void setup() {}

  public void setLocation(final int x, final int y) {
    int indicatorXLocation = x + 10;
    int indicatorYLocation = y + 10;

    while (ImageDisplay.defaultImageWidth < indicatorXLocation + ImageHoverPixelIndicator.indicatorWidth) {
      indicatorXLocation -= 1;
    }

    while (ImageDisplay.defaultImageHeight < indicatorYLocation + ImageHoverPixelIndicator.indicatorHeight) {
      indicatorYLocation -= 1;
    }

    this.component.setLocation(indicatorXLocation, indicatorYLocation);
    ((JLabel) this.component.getComponent(this.locationValueElementIndex)).setText(String.format("Location: (%d, %d)", x, y));
  }

  public void setRGBValue(final int red, final int green, final int blue) {
    ((JLabel) this.component.getComponent(this.rgbValueElementIndex)).setText(String.format("RGB Value: (%d, %d, %d)", red, green, blue));
  }

  public void setVisible(final boolean isVisible) {
    this.component.setVisible(isVisible);
  }
}
