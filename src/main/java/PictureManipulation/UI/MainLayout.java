
package main.java.PictureManipulation.UI;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.util.LinkedList;
import javax.swing.JFrame;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.util.TreeMap;

import main.java.PictureManipulation.Action.OpenImageAction;
import main.java.PictureManipulation.Action.SaveImageAction;
import main.java.PictureManipulation.Action.TogglePixelInfoOnImageHover;
import main.java.PictureManipulation.Action.OpenImageNegativeMenu;
import main.java.PictureManipulation.Action.OpenImageGreyscaleMenu;
import main.java.PictureManipulation.Action.OpenImageCroppingMenu;
import main.java.PictureManipulation.Action.OpenImageHistogramMenu;
import main.java.PictureManipulation.AppState;
import main.java.PictureManipulation.UI.MenuBar;
import main.java.PictureManipulation.UI.MenuBarMenu;
import main.java.PictureManipulation.UI.ImageDisplay;
import main.java.PictureManipulation.UI.ImageMetadataDisplay;
import main.java.PictureManipulation.UI.RGBSliderGroup;
import main.java.PictureManipulation.UI.ImageEffectToggleButtonGroup;

final public class MainLayout {
  AppState appState;
  GridBagLayout layout;
  GridBagConstraints gridBagConstraints;
  JFrame mainWindow;

  public MainLayout(final AppState appState, final JFrame mainWindow) {
    this.appState = appState;
    this.mainWindow = mainWindow;
    this.layout = new GridBagLayout();
    this.gridBagConstraints = new GridBagConstraints();

    this.setupLayout();
  }

  public GridBagLayout getLayout() { return this.layout; }

  void setupLayout() {
    this.setupMenuBar();

    this.setupTopLeftPanel();

    this.setupRightPanel();

    this.setupBottomControl();
  }

  void setupTopLeftPanel() {
    this.gridBagConstraints.weighty = 1.0;
    this.gridBagConstraints.weightx = 1.0;
    this.gridBagConstraints.gridheight = 1;

    ImageDisplay imageDisplay =
      new ImageDisplay(this.appState.getImageFile(), this.appState);

    this.layout.setConstraints(
      imageDisplay.internalComponent(), this.gridBagConstraints);

    this.mainWindow.add(imageDisplay.internalComponent());

    this.appState.listenTo(imageDisplay.reaction());
  }

  void setupRightPanel() {
    this.gridBagConstraints.weighty = 1.0;
    this.gridBagConstraints.weightx = 1.0;
    this.gridBagConstraints.gridheight = 1;
    this.gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;

    ImageMetadataDisplay imageMetadataDisplay = 
      new ImageMetadataDisplay(this.appState.getImageFile().getMetadata());

    this.layout.setConstraints(
      imageMetadataDisplay.internalComponent(), this.gridBagConstraints);

    this.mainWindow.add(imageMetadataDisplay.internalComponent());

    this.appState.listenTo(imageMetadataDisplay.reaction());
  }

  void setupMenuBar() {
    MenuBar menuBar = new MenuBar();

    LinkedList<TreeMap<String, AbstractAction>> fileMenus =
      this.fileMenu(menuBar.getFileChooser());

    MenuBarMenu menuBarMenu = new MenuBarMenu("File", fileMenus);

    menuBar.addMenu(menuBarMenu);

    LinkedList<TreeMap<String, AbstractAction>> pictureMenus =
      this.pictureMenu();

    MenuBarMenu pictureMenuBarMenu = new MenuBarMenu("Picture", pictureMenus);

    menuBar.addMenu(pictureMenuBarMenu);

    this.mainWindow.setJMenuBar(menuBar.internalComponent());
  }

  LinkedList<TreeMap<String, AbstractAction>> fileMenu(
    final JFileChooser fileChooser) {
    LinkedList<TreeMap<String, AbstractAction>> fileMenu =
      new LinkedList<TreeMap<String, AbstractAction>>();

    TreeMap<String, AbstractAction> openImageFileMenu =
      new TreeMap<String, AbstractAction>();

    final FileNameExtensionFilter imageFileFilter =
      new FileNameExtensionFilter("Image file", "jpg", "jpeg", "png", "bmp", "gif");
    
    final OpenImageAction openImageAction =
      new OpenImageAction(
        this.appState,
        fileChooser,
        imageFileFilter
      );

    final SaveImageAction saveImageAction = new SaveImageAction(this.appState);

    openImageFileMenu.put("Open image", openImageAction);
    openImageFileMenu.put("Save image", saveImageAction);

    fileMenu.add(openImageFileMenu);

    return fileMenu;
  }

  LinkedList<TreeMap<String, AbstractAction>> pictureMenu() {
    LinkedList<TreeMap<String, AbstractAction>> pictureMenu =
      new LinkedList<TreeMap<String, AbstractAction>>();

    TreeMap<String, AbstractAction> pictureEffectMenu =
      new TreeMap<String, AbstractAction>();

    pictureEffectMenu.put("Image negative", new OpenImageNegativeMenu(this.appState));
    pictureEffectMenu.put("Image greyscale", new OpenImageGreyscaleMenu(this.appState));
    pictureEffectMenu.put("Image cropping", new OpenImageCroppingMenu(this.appState));
    pictureEffectMenu.put("Image histogram", new OpenImageHistogramMenu(this.appState));
    pictureEffectMenu.put("Toggle pixel indicate on image hover (ON)", new TogglePixelInfoOnImageHover(this.appState));

    pictureMenu.add(pictureEffectMenu);

    return pictureMenu;
  }

  void setupBottomControl() {
    this.gridBagConstraints.gridwidth = 2;
    this.gridBagConstraints.gridheight = 1;
    this.gridBagConstraints.weightx = 1.0;
    this.gridBagConstraints.weighty = 1.0;

    RGBSliderGroup rgbSliderGroup = new RGBSliderGroup(this.appState);

    this.layout.setConstraints(
      rgbSliderGroup.internalComponent(), this.gridBagConstraints);

    this.mainWindow.add(rgbSliderGroup.internalComponent());

    this.appState.listenTo(rgbSliderGroup.reaction());

    ImageEffectToggleButtonGroup toggleButtonGroup =
      new ImageEffectToggleButtonGroup(this.appState);

    this.layout.setConstraints(
      toggleButtonGroup.internalComponent(), this.gridBagConstraints);

    this.mainWindow.add(toggleButtonGroup.internalComponent());

    this.appState.listenTo(toggleButtonGroup.reaction());
  }
}
