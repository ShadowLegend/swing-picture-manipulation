
package main.java.PictureManipulation.UI;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

import main.java.PictureManipulation.DigitalPicture;
import main.java.PictureManipulation.Pixel;

final public class ImageHistogram extends JPanel {
  final DigitalPicture digitalPicture;

  final ColorChannel redCounts;
  final ColorChannel blueCounts;
  final ColorChannel greenCounts;

  final int axisXOffsetAsPixel = 50;
  final int axisYOffsetAsPixel = 50;

  final int axisLabelOffset = 10;

  final int panelWidth = 600;
  final int panelHeight = 600;

  final int axisScaleFactor = 2;

  abstract class AbstractColorChannel {
    protected Integer[] colorCounts;

    public abstract void setCount(final int colorValue);
  }

  class ColorChannel extends AbstractColorChannel {
    public int globalMaximum = 0;

    ColorChannel() {
      this.colorCounts = new Integer[256];
    }

    @Override
    public void setCount(final int colorValue) {
      final Integer value = this.colorCounts[colorValue];

      if (value != null) {
        this.colorCounts[colorValue] =
          this.colorCounts[colorValue] + 1;
      } else {
        this.colorCounts[colorValue] = 1;
      }

      if (this.colorCounts[colorValue] > this.globalMaximum) {
        this.globalMaximum = this.colorCounts[colorValue];
      }
    }

    public Integer[] getColorCounts() { return this.colorCounts; }
  }

  public ImageHistogram(final DigitalPicture digitalPicture) {
    this.digitalPicture = digitalPicture;

    this.redCounts = new ColorChannel();
    this.blueCounts = new ColorChannel();
    this.greenCounts = new ColorChannel();

    this.processPixels();
  }

  void processPixels() {
    Color color = null;

    for (final Pixel pixel: this.digitalPicture.getPixels()) {
      color = pixel.getColor();

      this.redCounts.setCount(color.getRed());
      this.greenCounts.setCount(color.getGreen());
      this.blueCounts.setCount(color.getBlue());
    }
  }

  void drawXLabel(Graphics graphic) {
    final int commonValue = this.panelHeight - this.axisYOffsetAsPixel;

    graphic.drawLine(
      this.axisXOffsetAsPixel,
      this.axisYOffsetAsPixel,
      this.axisXOffsetAsPixel,
      commonValue
    );

    graphic.drawString(
      "Color Occurance",
      this.axisLabelOffset,
      this.axisYOffsetAsPixel - this.axisLabelOffset
    );

    graphic.drawString(
      "0",
      this.axisXOffsetAsPixel - (this.axisLabelOffset * 2),
      commonValue + this.axisLabelOffset
    );
  }

  void drawYLabel(Graphics graphic) {
    final int commonValue = this.panelHeight - this.axisYOffsetAsPixel;

    graphic.drawLine(
      this.axisXOffsetAsPixel,
      commonValue,
      commonValue,
      commonValue
    );

    graphic.drawString(
      "RGB Value",
      commonValue - (this.axisLabelOffset * 2),
      commonValue + (this.axisLabelOffset * 2)
    );
  }

  void drawGraphInfo(Graphics graphic) {
    final int commonValue = 
      this.axisLabelOffset + this.axisXOffsetAsPixel + 20;

    graphic.drawString(
      "Color Value Global Max Occurance:",
      commonValue,
      this.axisLabelOffset + this.axisXOffsetAsPixel + 10
    );

    graphic.setColor(Color.red);
    graphic.drawString(
      String.format("Red: %d", this.redCounts.globalMaximum),
      commonValue,
      this.axisLabelOffset + this.axisXOffsetAsPixel + 30
    );

    graphic.setColor(Color.green);
    graphic.drawString(
      String.format("Green: %d", this.greenCounts.globalMaximum),
      commonValue,
      this.axisLabelOffset + this.axisXOffsetAsPixel + 50
    );

    graphic.setColor(Color.blue);
    graphic.drawString(
      String.format("Blue: %d", this.blueCounts.globalMaximum),
      commonValue,
      this.axisLabelOffset + this.axisXOffsetAsPixel + 70
    );
  }

  @Override
  public void paint(Graphics graphic) {
    this.drawXLabel(graphic);
    this.drawYLabel(graphic);

    this.drawGraphInfo(graphic);

    final int axisOffset = this.axisXOffsetAsPixel + this.axisLabelOffset;

    final int previousXPosition = this.axisXOffsetAsPixel;
    final int previousYPosition = this.panelHeight - this.axisYOffsetAsPixel;
    final int maxRGBValue = 256;

    int loopIndex = 0;

    int previousRedXPosition = previousXPosition;
    int previousRedYPosition = previousYPosition;

    int previousGreenXPosition = previousRedXPosition;
    int previousGreenYPosition = previousRedYPosition;

    int previousBlueXPosition = previousGreenXPosition;
    int previousBlueYPosition = previousGreenYPosition;

    Integer colorValue = 0;

    int value = 0;

    final int redValueNormalizeFactor =
      this.redCounts.globalMaximum / this.panelHeight;
    final int greenValueNormalizeFactor =
      this.greenCounts.globalMaximum / this.panelHeight;
    final int blueValueNormalizeFactor =
      this.blueCounts.globalMaximum / this.panelHeight;

    for (; loopIndex < maxRGBValue; ++loopIndex) {
      value = loopIndex * this.axisScaleFactor;

      colorValue = this.redCounts.getColorCounts()[loopIndex];
      if (colorValue != null) {
        colorValue = colorValue / (redValueNormalizeFactor * 2);

        graphic.setColor(Color.red);

        graphic.drawLine(
          previousRedXPosition,
          previousRedYPosition,
          value + axisOffset,
          this.panelHeight - colorValue - axisOffset
        );

        previousRedXPosition = value + axisOffset;
        previousRedYPosition = this.panelHeight - colorValue - axisOffset;
      }

      colorValue = this.greenCounts.getColorCounts()[loopIndex];
      if (colorValue != null) {
        colorValue = colorValue / (greenValueNormalizeFactor * 2);

        graphic.setColor(Color.green);

        graphic.drawLine(
          previousGreenXPosition,
          previousGreenYPosition,
          value + axisOffset,
          this.panelHeight - colorValue - axisOffset
        );

        previousGreenXPosition = value + axisOffset;
        previousGreenYPosition = this.panelHeight - colorValue - axisOffset;
      }

      colorValue = this.blueCounts.getColorCounts()[loopIndex];
      if (colorValue != null) {
        colorValue = colorValue / (blueValueNormalizeFactor * 2);

        graphic.setColor(Color.blue);

        graphic.drawLine(
          previousBlueXPosition,
          previousBlueYPosition,
          value + axisOffset,
          this.panelHeight - colorValue - axisOffset
        );
        
        previousBlueXPosition = value + axisOffset;
        previousBlueYPosition = this.panelHeight - colorValue - axisOffset;
      }
    }
  }
}
