
package main.java.PictureManipulation.UI;

import java.util.Map;
import java.util.TreeMap;
import java.util.LinkedList;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.AbstractAction;

import main.java.PictureManipulation.UI.UIComponent;

final public class MenuBarMenu extends UIComponent<JMenu> {
  public MenuBarMenu() { this.component = new JMenu(); }

  public MenuBarMenu(
    final String menuLabel,
    final String menuItemLabel,
    final AbstractAction action
  ) {
    this.component = new JMenu(menuLabel);

    this.addMenuItem(menuItemLabel, action);
  }

  public MenuBarMenu(
    final String menuLabel,
    final LinkedList<TreeMap<String, AbstractAction>> menuItems
  ) {
    this.component = new JMenu(menuLabel);

    for (final TreeMap<String, AbstractAction> menuItem : menuItems) {
      for (final Map.Entry<String, AbstractAction> entry : menuItem.entrySet()) {
        this.addMenuItem(entry.getKey(), entry.getValue());
      }
    }
  }

  public void addMenuItem(final String label, final AbstractAction action) {
    JMenuItem menuItem = new JMenuItem();

    menuItem.setAction(action);
    menuItem.setText(label);

    this.component.add(menuItem);
  }

  public JMenu internalComponent() { return this.component; }

  public void update() {}

  protected void setup() {}
}
