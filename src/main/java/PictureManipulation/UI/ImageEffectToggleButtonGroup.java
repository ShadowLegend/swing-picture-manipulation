
package main.java.PictureManipulation.UI;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import javax.swing.JCheckBox;
import javax.swing.BoxLayout;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JPanel;

import main.java.PictureManipulation.AppState;
import main.java.PictureManipulation.UI.UIComponent;
import main.java.PictureManipulation.Action.ActionIdentifier;
import main.java.PictureManipulation.UI.ImageDimensionInput;

final public class ImageEffectToggleButtonGroup extends UIComponent<JPanel> {
  AppState appState;

  final String[] toggleOptions = new String[] {"grayscale", "negate"};

  final private class ImageChangeReaction implements PropertyChangeListener {
    @Override
    public void propertyChange(PropertyChangeEvent event) {
      switch (event.getPropertyName()) {
        case ActionIdentifier.OPENED_IMAGE_FILE: {
          ImageEffectToggleButtonGroup.this.component.removeAll();

          ImageEffectToggleButtonGroup.this.setup(
            ImageEffectToggleButtonGroup.this.toggleOptions);

          ImageEffectToggleButtonGroup.this.component.revalidate();
          ImageEffectToggleButtonGroup.this.component.updateUI();

          break;
        }

        default: {break;}
      }
    }
  }

  public ImageChangeReaction reaction() { return new ImageChangeReaction(); }

  final private class CheckBoxOnChange implements ItemListener {
    final String checkboxId;

    public CheckBoxOnChange(final String checkboxId) {
      this.checkboxId = checkboxId;
    }

    @Override
    public void itemStateChanged(ItemEvent event) {
      if (!ImageEffectToggleButtonGroup.this.appState.getImageFile().isImageRead()) {
        return;
      }

      final String action =
        this.checkboxId == "grayscale"
          ? ActionIdentifier.IMAGE_GRAYSCALE_TOGGLE
          : ActionIdentifier.IMAGE_NEGATE_TOGGLE;

      if (this.checkboxId == "grayscale") {
        ImageEffectToggleButtonGroup.this.appState.getDigitalPicture().grayscale();

        ImageEffectToggleButtonGroup
        .this
        .appState
        .dispatcher()
        .firePropertyChange(
          action,
          event.getStateChange() == ItemEvent.SELECTED,
          event.getStateChange() == ItemEvent.SELECTED
            ? ImageEffectToggleButtonGroup.this.appState.getDigitalPicture().getImageGrayscaled()
            : ImageEffectToggleButtonGroup.this.appState.getImageFile().getBufferedImage()
        );
      } else {
        ImageEffectToggleButtonGroup.this.appState.getDigitalPicture().negate();

        ImageEffectToggleButtonGroup
        .this
        .appState
        .dispatcher()
        .firePropertyChange(
          action,
          event.getStateChange() == ItemEvent.SELECTED,
          ImageEffectToggleButtonGroup.this.appState.getImageFile()
        );
      }
    }
  }

  public ImageEffectToggleButtonGroup(final AppState appState) {
    this.appState = appState;

    this.component = new JPanel();

    this.component.setLayout(new BoxLayout(this.component, BoxLayout.PAGE_AXIS));

    this.setup(this.toggleOptions);
  }

  public void update() {}

  protected void setup() {}

  protected void setup(final String[] toggleOptions) {
    JCheckBox checkbox;

    for (final String toggleOption: toggleOptions) {
      checkbox = new JCheckBox(toggleOption);

      checkbox.addItemListener(new CheckBoxOnChange(toggleOption));

      this.component.add(checkbox);
    }

    final int minWidth = this.appState.getImageFile().getMetadata().width;
    final int minHeight = this.appState.getImageFile().getMetadata().height;

    ImageDimensionInput dimensionInput =
      new ImageDimensionInput(minWidth, minHeight, this.appState);

    this.component.add(dimensionInput.internalComponent());
  }

  public JPanel internalComponent() { return this.component; }
}
