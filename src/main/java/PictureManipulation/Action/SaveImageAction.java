
package main.java.PictureManipulation.Action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;

import main.java.PictureManipulation.AppState;

final public class SaveImageAction extends AbstractAction {
  AppState appState;
  JFileChooser fileChooser;

  public SaveImageAction(final AppState appState) {
    this.appState = appState;

    this.fileChooser = new JFileChooser();

    this.fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

    this.fileChooser.setAcceptAllFileFilterUsed(false);
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    final int openState = this.fileChooser.showDialog(null, "Save Here");

    if (openState == JFileChooser.APPROVE_OPTION) {
      this.appState
        .getDigitalPicture()
        .saveImage(this.fileChooser.getCurrentDirectory().getAbsolutePath());
    }
  }
}
