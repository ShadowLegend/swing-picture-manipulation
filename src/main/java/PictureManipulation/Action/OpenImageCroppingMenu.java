
package main.java.PictureManipulation.Action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JFrame;

import main.java.PictureManipulation.AppState;
import main.java.PictureManipulation.UI.ImageCroppingDialog;

final public class OpenImageCroppingMenu extends AbstractAction {
  final AppState appState;

  public OpenImageCroppingMenu(final AppState appState) {
    this.appState = appState;
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    final JFrame frame = new JFrame();

    frame.add((new ImageCroppingDialog(this.appState)).internalComponent());

    frame.setVisible(true);
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    frame.setSize(
      this.appState.getImageFile().getMetadata().width,
      this.appState.getImageFile().getMetadata().height
    );
    frame.setResizable(false);
  }
}