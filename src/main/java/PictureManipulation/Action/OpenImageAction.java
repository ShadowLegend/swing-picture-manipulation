
package main.java.PictureManipulation.Action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import main.java.PictureManipulation.Model.ImageFile;
import main.java.PictureManipulation.Action.ActionIdentifier;
import main.java.PictureManipulation.AppState;
import main.java.PictureManipulation.ReturnResult;

final public class OpenImageAction extends AbstractAction {
  AppState appState;
  JFileChooser fileChooser;
  final FileNameExtensionFilter fileExtensionFilter;

  public OpenImageAction(
    final AppState appState,
    final JFileChooser fileChooser,
    final FileNameExtensionFilter fileExtensionFilter
  ) {
    this.appState = appState;
    this.fileChooser = fileChooser;
    this.fileExtensionFilter = fileExtensionFilter;
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    fileChooser.setFileFilter(this.fileExtensionFilter);

    final int openState = fileChooser.showDialog(null, "Open image");

    if (openState == JFileChooser.APPROVE_OPTION) {
      ImageFile imageFile = this.appState.getImageFile();

      final ReturnResult result =
        imageFile.openImage(fileChooser.getSelectedFile());

      if (result.isOk) {
        this.appState.getDigitalPicture().setImageFile(imageFile);

        this.appState.dispatcher().firePropertyChange(
          ActionIdentifier.OPENED_IMAGE_FILE,
          null,
          this.appState.getImageFile()
        );
      }
    }
  }
}
