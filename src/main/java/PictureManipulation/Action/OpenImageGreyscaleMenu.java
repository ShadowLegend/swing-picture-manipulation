
package main.java.PictureManipulation.Action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

import main.java.PictureManipulation.AppState;

final public class OpenImageGreyscaleMenu extends AbstractAction {
  final AppState appState;

  public OpenImageGreyscaleMenu(final AppState appState) {
    this.appState = appState;
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    System.out.println("Open image greyscale menu");
  }
}