
package main.java.PictureManipulation.Action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JFrame;

import main.java.PictureManipulation.AppState;
import main.java.PictureManipulation.UI.ImageHistogram;

final public class OpenImageHistogramMenu extends AbstractAction {
  final AppState appState;

  public OpenImageHistogramMenu(final AppState appState) {
    this.appState = appState;
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    final ImageHistogram imageHistogram =
      new ImageHistogram(this.appState.getDigitalPicture());

    final JFrame jFrame = new JFrame();

    jFrame.add(imageHistogram);

    jFrame.setSize(620, 620);
    jFrame.setResizable(false);
    jFrame.setVisible(true);
    jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
  }
}