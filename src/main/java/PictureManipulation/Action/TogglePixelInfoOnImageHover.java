package main.java.PictureManipulation.Action;

import java.awt.event.ActionEvent;
import javax.swing.JMenuItem;
import javax.swing.AbstractAction;

import main.java.PictureManipulation.AppState;

final public class TogglePixelInfoOnImageHover extends AbstractAction {
  AppState appState;

  public TogglePixelInfoOnImageHover(final AppState appState) {
    this.appState = appState;
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    JMenuItem menuItem = (JMenuItem) event.getSource();

    this.appState.isIndicatePixelInfoOnImageHover =
      !this.appState.isIndicatePixelInfoOnImageHover;

    if (this.appState.isIndicatePixelInfoOnImageHover) {
      menuItem.setText("Toggle pixel indicate on image hover (ON)");
    } else {
      menuItem.setText("Toggle pixel indicate on image hover (OFF)");
    }

    menuItem.revalidate();
    menuItem.updateUI();
  }
}