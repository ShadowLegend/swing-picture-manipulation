
package main.java.PictureManipulation.Action;

final public class ActionIdentifier {
  public static final String OPENED_IMAGE_FILE = "OPENED_IMAGE_FILE";

  public static final String ADJUST_IMAGE_COLOR = "ADJUST_IMAGE_COLOR";

  public static final String IMAGE_GRAYSCALE_TOGGLE = "IMAGE_GRAYSACLE_TOGGLE";

  public static final String IMAGE_NEGATE_TOGGLE = "IMAGE_NEGATE_TOGGLE";

  public static final String SAVE_IMAGE_FILE = "SAVE_IMAGE_FILE";
}
