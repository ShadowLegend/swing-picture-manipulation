
package main.java.PictureManipulation;

import java.awt.Color;

final public class Pixel {
  int positionX;
  int positionY;
  Color color;

  public Pixel(final int positionX, final int positionY, final Color color) {
    this.positionX = positionX;
    this.positionY = positionY;
    this.color = color;
  }

  public Pixel(final int positionX, final int positionY, final int rgbColor) {
    this.positionX = positionX;
    this.positionY = positionY;
    this.color = new Color(rgbColor);
  }

  public int getPositionX() { return this.positionX; }
  public int getPositionY() { return this.positionY; }
  public Color getColor() { return this.color; }

  public void setColor(final Color color) { this.color = color; }
}
